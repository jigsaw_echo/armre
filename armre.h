#ifndef _ARMRE_H
#define _ARMRE_H

#include <stdio.h>
#include <stdint.h>

typedef enum {
    OP_NONE = 0,
    OP_LDR_PC,
    OP_LDR_R,
    OP_LDR_IMM,
    OP_ADD_PC
} inst_desc_op;

struct inst_desc;

typedef struct inst_desc {
    inst_desc_op op;
    uint16_t    bits;
    uint16_t    rt;
    uint16_t    rm;
    uint16_t    rn;
    uint16_t    rd;
    uint32_t    imm;
    uint32_t    addr;
    struct inst_desc *next;
} inst_desc_t;

struct func;

typedef struct func {
    uint32_t    start;
    uint32_t    size;
    uint32_t    cnt;
    char        *name;
    inst_desc_t *idesc;
    uint16_t    *raw;
    struct func *next;
} func_t;

struct func_desc;

typedef struct func_desc {
    uint32_t    start;
    uint32_t    end;
    struct func_desc *next;
} func_desc_t;

typedef struct {
    uint32_t    start;
    uint32_t    size;
    uint32_t    cnt;
    uint32_t    *entries;
} got_t;

typedef struct {
    uint32_t    value;
    uint32_t    size;
    uint32_t    type;   /* See SYM_IS_FUNC */
    char        *name;
} sym_t;

#define SYM_IS_FUNC(__s) ((__s)->type == 1)

typedef struct {
    uint32_t    start;
    uint32_t    end;
    char        *data;
} rodata_t;

typedef struct {
    uint32_t    cnt;
    sym_t       *symbols;
} dynsym_t;

struct run_result;

typedef struct run_result {
    inst_desc_t *idesc;
    uint32_t    loaded_value;
    char        *loaded_sym;
    char        *loaded_str;
    struct run_result *next;
} run_result_t;

/**
 * @param fun           Given function
 * @param got           Entry of Global Offset Table
 * @param dyn           The .dynsym table
 * @param results       The output of execution
 * @return              -1 on error, 0 on success
 */
int
run_func(func_t *func,
         got_t *got,
         dynsym_t *dyn,
         rodata_t *rod,
         run_result_t **results);

/**
 * @param fp            File handle
 * @param func          The root of functions
 * @param got           Entry of Global Offset Table
 * @param dyn           The .dynsym table
 * @return              -1 on error, 0 on success
 */
int
parse_elf(FILE *fp,
          func_t **root,
          got_t *got,
          dynsym_t *dyn,
          rodata_t *rod,
          func_desc_t *fdesc);

/**
 * @param func          The function to be decoded.
 * @return              -1 on error, 0 on success
 */
int
decode_func(func_t *func);

#endif
