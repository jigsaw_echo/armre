#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "armre.h"

/*
 * Load from Literal Pool
 * 01001 - ttt - iiiiiiii
 * 0x4800
 * ldr Rt, [PC, imm8]
 * Rt = *(PC + (imm8 * 4))
 *
 * Load(register)
 * 0101 - 100 - mmm - nnn - ttt
 * 0x5800
 * ldr Rt, [Rn, Rm]
 * Rt = *(Rn + Rm)
 *
 * Load(imm)
 * 011 - 0 - 1 - mmmmm - nnn - ttt
 * 0x6800
 * ldr Rt, [Rn, imm]
 * Rt = *(Rn + (imm * 4))
 *
 * Add PC
 * 010001 - 00 - 0 - 1111 - ddd
 * 0x4478
 * add Rd, PC
 * Rd += PC
 *
 */

#define LDR_PC      ((uint16_t)0x9)
#define LDR_R       ((uint16_t)0x2C)
#define LDR_IMM     ((uint16_t)0xD)
#define ADD_PC      ((uint16_t)0x88F)

#define IS_LDR_PC(b)    (((b) >> 11) == LDR_PC)
#define IS_LDR_R(b)     (((b) >> 9) == LDR_R)
#define IS_LDR_IMM(b)   (((b) >> 11) == LDR_IMM)
#define IS_ADD_PC(b)    (((b) >> 3) == ADD_PC)

typedef struct {
    union {
        uint16_t bits;

        struct {
            uint16_t imm : 8;
            uint16_t rt  : 3;
            uint16_t fix : 5;
        } ldr_pc;

        struct {
            uint16_t rt  : 3;
            uint16_t rn  : 3;
            uint16_t rm  : 3;
            uint16_t fix : 7;
        } ldr_r;

        struct {
            uint16_t rt  : 3;
            uint16_t rn  : 3;
            uint16_t imm : 5;
            uint16_t fix : 5;
        } ldr_imm;

        struct {
            uint16_t rd  : 3;
            uint16_t fix : 13;
        } add_pc;
    } u;
} inst_t;

static inst_desc_t *
decode_instruct(uint16_t bits)
{
    inst_t ins;
    inst_desc_t *idesc;
    inst_desc_op op = OP_NONE;

    if (IS_LDR_PC(bits)) {
        op = OP_LDR_PC;
    } else if (IS_LDR_R(bits)) {
        op = OP_LDR_R;
    } else if (IS_LDR_IMM(bits)) {
        op = OP_LDR_IMM;
    } else if (IS_ADD_PC(bits)) {
        op = OP_ADD_PC;
    }

    if (op == OP_NONE)
        return NULL;

    ins.u.bits = bits;
    idesc = calloc(1, sizeof(inst_desc_t));
    idesc->bits = bits;
    idesc->op = op;

    switch (op) {
    case OP_LDR_PC:
        idesc->rt = ins.u.ldr_pc.rt;
        idesc->imm = ins.u.ldr_pc.imm * 4;
        break;

    case OP_LDR_R:
        idesc->rm = ins.u.ldr_r.rm;
        idesc->rn = ins.u.ldr_r.rn;
        idesc->rt = ins.u.ldr_r.rt;
        break;

    case OP_LDR_IMM:
        idesc->imm = ins.u.ldr_imm.imm * 4;
        idesc->rn = ins.u.ldr_imm.rn;
        idesc->rt = ins.u.ldr_imm.rt;
        break;

    case OP_ADD_PC:
        idesc->rd = ins.u.add_pc.rd;
        break;

    default:
        /* Just to make compiler happy */
        return NULL;
    }

    return idesc;
}

int
decode_func(func_t *func)
{
    int i;
    uint16_t *bits;
    uint32_t addr;
    inst_desc_t *idesc, *tail;

    bits = func->raw;
    addr = func->start;
    tail = NULL;

    for (i = 0; i < func->cnt; i++, addr += 2) {
        if (bits[i] >> 13 == 0x7 &&
            ((bits[i] >> 11) & 0x3) != 0) {
            /* Skip 32bit Thumb */
            i += 1;
            addr += 2;
            continue;
        }

        idesc = decode_instruct(bits[i]);
        if (idesc) {
            idesc->addr = addr;
            if (!func->idesc)
                func->idesc = idesc;
            if (tail)
                tail->next = idesc;
            tail = idesc;
        }
    }

    return 0;
}
