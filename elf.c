#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include <errno.h>

#include "armre.h"
#include "elf.h"

static int
parse_got(got_t *got, Elf32_Shdr *sh, char *sec)
{
    got->start = sh->sh_addr;
    got->size = sh->sh_size;
    got->cnt = got->size / sizeof(uint32_t);
    got->entries = (uint32_t *) sec;

    return 0;
}

static int
parse_dyn(dynsym_t *dyn, Elf32_Shdr *sh, char *sec, char *strtab)
{
    Elf32_Sym *es;
    int i, cnt;
    sym_t *s;

    cnt = sh->sh_size / sizeof(Elf32_Sym);
    s = calloc(cnt, sizeof(sym_t));
    dyn->symbols = s;
    dyn->cnt = cnt - 1;
    es = (Elf32_Sym *) sec;

    for (i = 1; i < cnt; i++, s++, es++) {
        s->value = es->st_value;
        s->name = &strtab[es->st_name];
        s->size = es->st_size;
        s->type = ELF_ST_TYPE(es->st_info) == STT_FUNC;
    }

    return 0;
}

static int
parse_text(func_t **root,
           Elf32_Shdr *sh,
           char *sec,
           dynsym_t *dyn,
           func_desc_t *fdesc)
{
    int i;
    sym_t *s;
    func_t *curr, *tail;

    s = dyn->symbols;
    curr = tail = NULL;

    for (i = 0; i < dyn->cnt; i++, s++) {
        if (!SYM_IS_FUNC(s))
            continue;
        if (s->size == 0 || s->value == 0)
            continue;
        /* Only Thumb 16bits allowd */
        if ((s->value & 1) == 0)
            continue;

        curr = calloc(1, sizeof(func_t));
        curr->name = s->name;
        curr->start = s->value - 1;
        curr->size = s->size;
        curr->raw = (uint16_t *) (sec + curr->start - sh->sh_addr);
        curr->cnt = curr->size / sizeof(uint16_t);

        curr->next = tail;
        tail = curr;
    }

    while (fdesc) {
        char *fname;

        curr = calloc(1, sizeof(func_t));
        fname = calloc(1, 8);
        sprintf(fname, "%x", fdesc->start);
        curr->name = fname;
        curr->start = fdesc->start;
        curr->size = fdesc->end - curr->start;
        curr->raw = (uint16_t *) (sec + curr->start - sh->sh_addr);
        curr->cnt = curr->size / sizeof(uint16_t);

        curr->next = tail;
        tail = curr;

        fdesc = fdesc->next;
    }

    if (!curr)
        return -1;

    *root = curr;

    return 0;
}

int
parse_elf(FILE *fp,
          func_t **root,
          got_t *got,
          dynsym_t *dyn,
          rodata_t *rod,
          func_desc_t *fdesc)
{
    Elf32_Ehdr ehdr;
    Elf32_Shdr *shdrs, *shstr, *psh, *sh_dyn, *sh_text, *sh_got, *sh_rod;
    char *shstab, *dynstab, *sec_dyn, *sec_text, *sec_got, *sec_rod;
    int ret, i;

    sh_dyn = sh_text = sh_got = sh_rod = NULL;
    shstab = dynstab = sec_dyn = sec_text = sec_got = sec_rod = NULL;

    fseek(fp, 0, SEEK_SET);
    /* read file header */
    ret = fread(&ehdr, sizeof(ehdr), 1, fp);
    if (ret != 1)
        goto out;

    fseek(fp, ehdr.e_shoff, SEEK_SET);
    shdrs = calloc(ehdr.e_shnum, ehdr.e_shentsize);
    /* read all section headers */
    ret = fread(shdrs, ehdr.e_shentsize, ehdr.e_shnum, fp);
    if (ret != ehdr.e_shnum)
        goto out;

    shstr = &shdrs[ehdr.e_shstrndx];
    shstab = calloc(1, shstr->sh_size);
    fseek(fp, shstr->sh_offset, SEEK_SET);
    /* read section name string table */
    ret = fread(shstab, shstr->sh_size, 1, fp);
    if (ret != 1)
        goto out;

    psh = shdrs;
    /* .dynstr, .dynsym, .text, .got, .rodata */
    for (i = 0; i < ehdr.e_shnum; i++, psh++) {
        char **sec = NULL;

        if (strcmp(&shstab[psh->sh_name], ".dynsym") == 0) {
            sec = &sec_dyn;
            sh_dyn = psh;
        } else if (strcmp(&shstab[psh->sh_name], ".dynstr") == 0) {
            sec = &dynstab;
        } else if (strcmp(&shstab[psh->sh_name], ".text") == 0) {
            sec = &sec_text;
            sh_text = psh;
        } else if (strcmp(&shstab[psh->sh_name], ".got") == 0) {
            sec = &sec_got;
            sh_got = psh;
        } else if (strcmp(&shstab[psh->sh_name], ".rodata") == 0) {
            sec = &sec_rod;
            sh_rod = psh;
        }

        if (sec) {
            *sec = calloc(1, psh->sh_size);
            fseek(fp, psh->sh_offset, SEEK_SET);
            ret = fread(*sec, psh->sh_size, 1, fp);
            if (ret != 1)
                goto out;
        }
    }

    if (!sec_dyn || !dynstab || !sec_text || !sec_got || !sec_rod) {
        printf("section missing\n");
        return -1;
    }

    rod->start = sh_rod->sh_addr;
    rod->end = sh_rod->sh_addr + sh_rod->sh_size;
    rod->data = sec_rod;

    if (parse_got(got, sh_got, sec_got) == -1) {
        printf("parse got fail\n");
        return -1;
    }
    if (parse_dyn(dyn, sh_dyn, sec_dyn, dynstab) == -1) {
        printf("parse dyn fail\n");
        return -1;
    }

    if (parse_text(root, sh_text, sec_text, dyn, fdesc) == -1) {
        printf("parse dyn fail\n");
        return -1;
    }

    return 0;

out:
    perror("parse_elf");

    return -1;
}
