#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

#include "armre.h"

int
main(int argc, char **argv)
{
    FILE *fp;
    func_t *head, *curr;
    got_t got;
    dynsym_t dyn;
    rodata_t rod;
    int i, n;
    func_desc_t *fde, *ftail;
    FILE *fout;

    if (argc != 3) {
        printf("Usage: %s <ELF> <OUTPUT>\n", argv[0]); 
        return -1;
    }

    if (!(fp = fopen(argv[1], "r"))) {
        printf("Cannot open file %s\n", argv[1]);
        return -1;
    }
    if (!(fout = fopen(argv[2], "w"))) {
        printf("Cannot open file %s\n", argv[1]);
        return -1;
    }

    /* read extra func which is not in dynsym */
    printf("Read extra func which is not in dynsym.\n");
    fde = ftail = NULL;
    do {
        uint32_t start, end;

        printf("FUNC start (Enter 0 to skip):\n");
        n = scanf("%x", &start);
        if (start == 0)
            break;
        printf("FUNC end (Enter 0 to skip):\n");
        n = scanf("%x", &end);
        if (end == 0)
            break;
        fde = malloc(sizeof(*fde));
        fde->start = start;
        fde->end = end;
        fde->next = ftail;
        ftail = fde;
    } while (1);

    if (parse_elf(fp,
                  &head,
                  &got,
                  &dyn,
                  &rod,
                  fde) == -1) {
        printf("Cannot parse file %s\n", argv[1]);
        return -1;
    }

    fclose(fp);

    curr = head;
    do {
        run_result_t *res = NULL;

        if (decode_func(curr) == -1) {
            printf("Decode %s fail\n", curr->name);
            return -1;
        }
        if (run_func(curr, &got, &dyn, &rod, &res) == -1) {
            printf("Run %s failure\n", curr->name);
            return -1;
        }
        if (res) {
            fprintf(fout, "FUNC [%s]\n", curr->name);
            do {
                fprintf(fout, "  %08x:%04x  %08x",
                        res->idesc->addr, res->idesc->bits,
                        res->loaded_value);
                if (res->loaded_sym) {
                    fprintf(fout, ":sym:[%s]\n", res->loaded_sym);
                } else if (res->loaded_str) {
                    fprintf(fout, ":str:[%s]\n", res->loaded_str);
                } else
                    fprintf(fout, "\n");
                res = res->next;
            } while (res);
            fprintf(fout, "\n");
        }

        curr = curr->next;
    } while (curr);

    return 0;
}
