#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "armre.h"

typedef struct {
    char init[16];
    uint32_t registers[16];
} regs_t;

static regs_t g_regs;

#define R(n)    (g_regs.registers[(n)])
#define SP      R(13)
#define LR      R(14)
#define PC      R(15)
#define INIT(n) (g_regs.init[(n)])

#define STORE(n, v) do { \
    R(n) = (v); \
    INIT(n) = 1; \
} while (0)

static char *
lookup_sym(dynsym_t *dyn, uint32_t val)
{
    sym_t *s;
    int i;

    s = dyn->symbols;
    for (i = 0; i < dyn->cnt; i++, s++) {
        if (s->value == val)
            return s->name;
    }

    return NULL;
}

static char *
lookup_str(rodata_t *rod, uint32_t addr)
{
    if (addr >= rod->start && addr < rod->end)
        return &rod->data[addr - rod->start];

    return NULL;
}

static void
run_ldr_pc(inst_desc_t *idesc, func_t *func, uint32_t *val)
{
    uint32_t addr, offs, vrt;

    addr = idesc->addr + idesc->imm + 4;
    addr &= ~3;
    offs = (addr - func->start) / 2;
    vrt = func->raw[offs] | ((uint32_t)func->raw[offs + 1] << 16);
    *val = vrt;

    STORE(idesc->rt, vrt);
}

static int
run_ldr_r(inst_desc_t *idesc,
          got_t *got,
          dynsym_t *dyn,
          rodata_t *rod,
          uint32_t *val,
          char **pname)
{
    uint32_t rt, rn, rm, vrt, addr, offs;
    char *name;

    rt = idesc->rt;
    rn = idesc->rn;
    rm = idesc->rm;

    if (!INIT(rn) || !INIT(rm))
        return -1;

    addr = R(rn) + R(rm);
    /* check if addr is in GOT */
    if (addr < got->start || addr > (got->start + got->size))
        return -1;

    offs = (addr - got->start) / 4;
    vrt = got->entries[offs];
    STORE(rt, vrt);
    *val = vrt;

    name = lookup_sym(dyn, vrt);
    *pname = name;

    return 0;
}

static int
run_add_pc(inst_desc_t *idesc, rodata_t *rod, uint32_t *val, char **pname)
{
    uint32_t rd, vrd;
    char *name;

    rd = idesc->rd;
    if (!INIT(rd))
        return -1;

    vrd = R(rd) + idesc->addr + 4;
    /* XXX do we need to mask the bits? */
    //vrd &= ~3;
    STORE(rd, vrd);
    *val = vrd;
    name = lookup_str(rod, vrd);
    *pname = name;
    if (name) {
        /* remove the \n */
        while (*name) {
            if (*name == '\n')
                *name = 0;
            name++;
        }
    }

    return 0;
}

static int
run_ldr_imm(inst_desc_t *idesc,
            got_t *got,
            dynsym_t *dyn,
            rodata_t *rod,
            uint32_t *val,
            char **pname)
{
    uint32_t rt, rn, vrt, addr, offs, imm;
    char *name;

    rt = idesc->rt;
    rn = idesc->rn;
    imm = idesc->imm;

    if (!INIT(rn))
        return -1;

    addr = R(rn) + imm;
    /* check if addr is in GOT */
    if (addr < got->start || addr > (got->start + got->size))
        return -1;

    offs = (addr - got->start) / 4;
    vrt = got->entries[offs];
    STORE(rt, vrt);
    *val = vrt;

    name = lookup_sym(dyn, vrt);
    *pname = name;

    return 0;
}

/*
 * This function tries to parse references to global dynamic symbols.
 * Therefore, only memory and arithmetic operations are parsed.
 * At this moment, only ADD and LDR are checked.
 */
int
run_func(func_t *func,
         got_t *got,
         dynsym_t *dyn,
         rodata_t *rod,
         run_result_t **results)
{
    inst_desc_t *idesc;
    int i;
    run_result_t *curr, *head, *tail;

    bzero(&g_regs, sizeof(g_regs));

    idesc = func->idesc;
    curr = head = tail = NULL;

    while (idesc) {
        char *name = NULL;
        uint32_t loaded_value = 0;

        switch(idesc->op) {
        case OP_LDR_PC:
            run_ldr_pc(idesc, func, &loaded_value);
            curr = calloc(1, sizeof(run_result_t));
            break;

        case OP_LDR_R:
            if (!run_ldr_r(idesc, got, dyn, rod, &loaded_value, &name)) {
                curr = calloc(1, sizeof(run_result_t));
                curr->loaded_sym = name;
            }
            break;

        case OP_LDR_IMM:
            if (!run_ldr_imm(idesc, got, dyn, rod, &loaded_value, &name)) {
                curr = calloc(1, sizeof(run_result_t));
                curr->loaded_sym = name;
            }
            break;

        case OP_ADD_PC:
            if (!run_add_pc(idesc, rod, &loaded_value, &name)) {
                curr = calloc(1, sizeof(run_result_t));
                curr->loaded_str = name;
            }
            break;

        default:
            printf("Unkown instruction [%s]:    %08x:%04x\n",
                   func->name, idesc->addr, idesc->bits);
            return -1;
        }

        if (curr) {
            curr->idesc = idesc;
            curr->loaded_value = loaded_value;
            if (tail)
                tail->next = curr;
            if (!head)
                head = curr;
            tail = curr;
            curr = NULL;
        }
        idesc = idesc->next;
    }

    *results = head;

    return 0;
}
